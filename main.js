/*MIT License

Copyright (c) 2019 Elijah Gregg

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.*/

try {
	// initialization
	var styling = false;
	var saveStyles = (!!localStorage.getItem('webStyles')) ? JSON.parse(localStorage.getItem('webStyles')) : [];
	// load saved styles
	if (saveStyles.length) {
		for (i = 0; i < saveStyles.length; i++) {
			createStyle(saveStyles[i]);
		}
	}
	// create style element for highlighted elements
	var highlight = document.createElement('style');
	highlight.innerHTML = '.highlight {outline: 1px solid blue; cursor: default; box-sizing: border-box;}';
	document.getElementsByTagName('head')[0].appendChild(highlight);
	// create div to alert users that Web Styler has been successfully injected
	var mainDiv = document.createElement('div');
	mainDiv.innerHTML = '<div style="position: fixed; z-index: 1; left: 0; top: 0; display: block; overflow: hidden; background-color: rgba(0,0,0,0.4); width: 100%; height: 100%;"> <div style="background-color: white; margin: 15% auto; padding: 20px; border: 1px solid black; width: 80%;"> <h1 text-align="center">Web Styler</h1> <p>Welcome to Web Styler! To style any element simply press &ltCtrl&gt&ltAlt&gt&ltS&gt. Then, select the element you wish to style and modify its CSS! To delete all saved settings, press &ltCtrl&gt&ltAlt&gt&ltA&gt.</p> <br /> <h6><i>Click anywhere to close</i></h6> </div> </div>'; 
	document.body.appendChild(mainDiv);
	// intercept keyboard shortcut
	document.onkeyup = function(e) {
		if (e.key == 'a' && e.ctrlKey && e.altKey) {
			removeStyles();
		}
		if (e.key == 's' && e.ctrlKey && e.altKey) {
			stylePage();
		}
	}
	// intercept all clicks on page
	document.onclick = function(e) {
		// hide the main div
		if (mainDiv.style.display !== "none") {
			mainDiv.style.display = "none";
		}
		// select an element if currently styling
		if (styling) {
			e.preventDefault();
			styling = false;
			styleElement(e.target);
		}
	};
	// style the page when a user clicks the defined keyboard shortcut
	function stylePage() {
		styling = true;
	}
	// add border to hovered element if the page is currently being styled
	window.addEventListener('mouseover', function(e) {
		if (styling) {
			e.target.classList.add('highlight');
		}
	});
	// remove border from element when it stops being hovered
	window.addEventListener('mouseout', function(e) {
		if (e.target.classList.contains('highlight')) {
			e.target.classList.remove('highlight');
		}
	});
	// style selected element
	function styleElement(e) {
		// create containing element for styling dialogue
		var styleContainer = document.createElement('div');
		styleContainer.width = '30vw';
		styleContainer.height = '35vh';
		styleContainer.position = 'fixed';
		styleContainer.bottom = '0';
		styleContainer.right = '0';
		styleContainer.id = 'styleContainer';
		// create styling dialogue
		var styleDialogue = document.createElement('textarea');
		styleDialogue.style.width = '30vw';
		styleDialogue.style.height = '30vh';
		styleDialogue.style.position = 'fixed';
		styleDialogue.style.bottom = '5vh';
		styleDialogue.style.right = '0';
		styleDialogue.id = 'styleDialogue';
		populateStyleInput(styleDialogue, e);
		styleContainer.appendChild(styleDialogue);
		// create button to exit styling dialogue
		var exitBtn = document.createElement('button');
		exitBtn.innerHTML = 'Done';
		exitBtn.style.width = '30vw';
		exitBtn.style.height = '5vh';
		exitBtn.style.position = 'fixed';
		exitBtn.style.bottom = '0';
		exitBtn.style.right = '0';
		exitBtn.onclick = applyStyles;
		styleContainer.appendChild(exitBtn);
		// append container to body
		document.body.appendChild(styleContainer);
	}
	// populate textarea with css classes and id
	function populateStyleInput(input, el) {
		var text = '';
		// get all element classes
		for (i = 0; i < el.classList.length; i++) {
			text += (el.classList[i] != 'highlight') ? '.'+el.classList[i]+' {\n    \n}\n' : '';
		}
		// get element id
		text += (!!el.id) ? '#'+el.id+' {\n    \n}\n' : '';
		// get element tag name
		text += el.tagName.toLowerCase()+' {\n    \n}\n';
		// add text to textarea
		input.innerHTML = text;
	}
	// apply styles to element
	function applyStyles() {
		var style = document.getElementById('styleDialogue').value;
		createStyle(style);
		saveStyles.push(style);
		localStorage.setItem('webStyles', JSON.stringify(saveStyles));
		document.getElementById('styleContainer').parentNode.removeChild(document.getElementById('styleContainer'));
	}
	// create style element
	function createStyle(text) {
		var styleEl = document.createElement('style');
		styleEl.innerHTML = text;
		styleEl.classList.add('genStyles');
		document.getElementsByTagName('head')[0].appendChild(styleEl);
	}
	// remove all styles
	function removeStyles() {
		localStorage.removeItem('webStyles');
		saveStyles = [];
		var styles = document.getElementsByTagName('style');
		for (i = 0; i < styles.length; i++) {
			if (styles[i].classList.contains('genStyles')) {
				styles[i].parentNode.removeChild(styles[i]);
			}
		}
	}
}
catch(err) {
	// alert users to any errors
	alert('Web Styler has encountered an error:\n'+err);
}
