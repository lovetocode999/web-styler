/*MIT License

Copyright (c) 2019 Elijah Gregg

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.*/
javascript:try{var styling=!1,saveStyles=localStorage.getItem("webStyles")?JSON.parse(localStorage.getItem("webStyles")):[];if(saveStyles.length)for(i=0;i<saveStyles.length;i++)createStyle(saveStyles[i]);var highlight=document.createElement("style");highlight.innerHTML=".highlight {outline: 1px solid blue; cursor: default; box-sizing: border-box;}",document.getElementsByTagName("head")[0].appendChild(highlight);var mainDiv=document.createElement("div");function stylePage(){styling=!0}function styleElement(e){var t=document.createElement("div");t.width="30vw",t.height="35vh",t.position="fixed",t.bottom="0",t.right="0",t.id="styleContainer";var l=document.createElement("textarea");l.style.width="30vw",l.style.height="30vh",l.style.position="fixed",l.style.bottom="5vh",l.style.right="0",l.id="styleDialogue",populateStyleInput(l,e),t.appendChild(l);var i=document.createElement("button");i.innerHTML="Done",i.style.width="30vw",i.style.height="5vh",i.style.position="fixed",i.style.bottom="0",i.style.right="0",i.onclick=applyStyles,t.appendChild(i),document.body.appendChild(t)}function populateStyleInput(e,t){var l="";for(i=0;i<t.classList.length;i++)l+="highlight"!=t.classList[i]?"."+t.classList[i]+" {\n    \n}\n":"";l+=t.id?"#"+t.id+" {\n    \n}\n":"",l+=t.tagName.toLowerCase()+" {\n    \n}\n",e.innerHTML=l}function applyStyles(){var e=document.getElementById("styleDialogue").value;createStyle(e),saveStyles.push(e),localStorage.setItem("webStyles",JSON.stringify(saveStyles)),document.getElementById("styleContainer").parentNode.removeChild(document.getElementById("styleContainer"))}function createStyle(e){var t=document.createElement("style");t.innerHTML=e,t.classList.add("genStyles"),document.getElementsByTagName("head")[0].appendChild(t)}function removeStyles(){localStorage.removeItem("webStyles"),saveStyles=[];var e=document.getElementsByTagName("style");for(i=0;i<e.length;i++)e[i].classList.contains("genStyles")&&e[i].parentNode.removeChild(e[i])}mainDiv.innerHTML='<div style="position: fixed; z-index: 1; left: 0; top: 0; display: block; overflow: hidden; background-color: rgba(0,0,0,0.4); width: 100%; height: 100%;"> <div style="background-color: white; margin: 15% auto; padding: 20px; border: 1px solid black; width: 80%;"> <h1 text-align="center">Web Styler</h1> <p>Welcome to Web Styler! To style any element simply press &ltCtrl&gt&ltAlt&gt&ltS&gt. Then, select the element you wish to style and modify its CSS! To delete all saved settings, press &ltCtrl&gt&ltAlt&gt&ltA&gt.</p> <br /> <h6><i>Click anywhere to close</i></h6> </div> </div>',document.body.appendChild(mainDiv),document.onkeyup=function(e){"a"==e.key&&e.ctrlKey&&e.altKey&&removeStyles(),"s"==e.key&&e.ctrlKey&&e.altKey&&stylePage()},document.onclick=function(e){"none"!==mainDiv.style.display&&(mainDiv.style.display="none"),styling&&(e.preventDefault(),styling=!1,styleElement(e.target))},window.addEventListener("mouseover",function(e){styling&&e.target.classList.add("highlight")}),window.addEventListener("mouseout",function(e){e.target.classList.contains("highlight")&&e.target.classList.remove("highlight")})}catch(e){alert("Web Styler has encountered an error:\n"+e)}
